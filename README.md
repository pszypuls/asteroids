# Asteroids game remake #

This repository contains a remake of a classic asteroids game written in Java.

### Details ###
- MVC design pattern
- multi-threaded
- Primitive view implemented in Swing java library, but design allows any graphic library to be used
- complete separation of update and render loops
- in principle, allows for more than one view at the same time.

### Future plans ###
- moving game settings into separate class
- better implementation of a view component, presumably in a more fitting library (libGDX/JavaFX)
- adding AI enemy