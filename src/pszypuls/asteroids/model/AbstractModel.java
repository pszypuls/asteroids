package pszypuls.asteroids.model;

import java.util.UUID;

import pszypuls.asteroids.model.mockup.AbstractMockup;
import pszypuls.asteroids.util.ToroidVector;
import pszypuls.asteroids.util.Vector;

/**
 * Abstract parent class for all object models contained in the game model.
 * 
 * @param <ObjectMockup> Parameter defining a type of mockup returned when getMockup method is executed.
 * This parameter must inherit from an AbstractMockup class
 */
public abstract class AbstractModel<ObjectMockup extends AbstractMockup>
{
	/**
	 * Unique identifier of this object.
	 */
	protected UUID id;

	/**
	 * Position of this asteroid in a toroidal space
	 */
	protected ToroidVector position;
	
	/**
	 * Represents a velocity vector in a regular 2D space.
	 */
	protected Vector velocity;
	
	/**
	 * Radius of a circle centered in a point described by position vector. 
	 * Specifies an area considered to be a part of this object.
	 * It is used to check for collisions with this object.
	 */
	protected double radius;


	/**
	 * Public constructor of this class. Initializes id with some unique value.
	 * Rest is initialized with default values.
	 */
	public AbstractModel()
	{
		id = UUID.randomUUID();
		position = new ToroidVector();
		velocity = new Vector();
		radius = 0;
	}
	
	/**
	 * Public constructor of this class. Initializes id with some unique value.
	 * @param position Specifies a location of a center of an object in a game space.
	 * @param velocity Specifies a velocity vector in a 2D space.
	 * @param radius Specifies a collision radius around the center of an object. Value in fractions of a space size.
	 */
	public AbstractModel(final ToroidVector position, final Vector velocity, final double radius)
	{
		id = UUID.randomUUID();
		this.position = position;
		this.velocity = velocity;
		this.radius = radius;
	}	
	
	/**
	 * Abstract method used to generate Mockups of an inherited object models
	 * Needs to overriden by any inheriting class
	 * @return Returns an object representing a mockup of an object of a type that executes this method
	 */
	public abstract ObjectMockup getMockup();
	
	/**
	 * Abstract method that defines the behaviour of an object of this type. 
	 * Needs to be overridden by any inheriting class. 
	 * @param deltaTime Determines how much time has passed since the last update of this object. 
	 * Specifies a flow of time in the game model.
	 */
	public abstract void update(final double deltaTime);
	
	
	/**
	 * Method that checks if this object collides with the one given as a parameter
	 * 
	 * @param abstractModel Specifies an AbstractModel object for which the collision is being checked.
	 * @return Returns true if a collision occurred, false otherwise.
	 */
	public boolean checkCollision(final AbstractModel<?> abstractModel)
	{
		//if distance between centers of both objects is smaller or equal to sum of radiuses then collision occurred
		if (position.distance(abstractModel.getPosition()) <= radius/2 + abstractModel.getRadius()/2)
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
	/**
	 * Getter method for a position vector of an object in toroidal space.
	 * @return Returns ToroidVector object representing a position of a center of this object in toroidal space. 
	 */
	public ToroidVector getPosition()
	{
		return position;
	}

	/**
	 * Setter method for a position vector of an object in toroidal space.
	 */
	public void setPosition(final ToroidVector position)
	{
		this.position = position;
	}

	/**
	 * Getter method for a velocity of an object.
	 * @return Returns velocity of an object.
	 */
	public Vector getVelocity()
	{
		return velocity;
	}

	/**
	 * Getter method for a velocity of an object.
	 */
	public void setVelocity(final Vector velocity)
	{
		this.velocity = velocity;
	}

	/**
	 * Getter method for a collision radius of an object.
	 * @return Returns collision radius of an object.
	 */
	public double getRadius()
	{
		return radius;
	}

	/**
	 * Getter method for a collision radius of an object.
	 */
	public void setRadius(final double radius)
	{
		this.radius = radius;
	}
	/**
	 * Getter method for unique identifier of this object.
	 * @return Returns unique identifier of this object
	 */
	public UUID getId()
	{
		return id;
	}
}
