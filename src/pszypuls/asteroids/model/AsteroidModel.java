package pszypuls.asteroids.model;

import pszypuls.asteroids.model.mockup.AsteroidMockup;
import pszypuls.asteroids.util.ToroidVector;
import pszypuls.asteroids.util.Vector;

/**
 * 
 * Class representing an asteroid in a game model. 
 * Encapsulates whole information and behaviour of an asteroid needed for the game model to operate on it.
 */
public class AsteroidModel extends AbstractModel<AsteroidMockup>
{
	/**
	 * Determines how big this asteroid currently is. Each received hit lowers this number by one.
	 * This value needs to be non-negative. Asteroid is destroyed when it drops to 0. 
	 */
	private int size;
	
	/**
	 * Public constructor for an AsteroidModel class.
	 * @param position location of a center of this object
	 * @param velocity velocity vector in a flat 2D space
	 * @param size value describing how big this asteroid is. It determines points gained after destroying it 
	 * as well as whether it will shatter after being hit or just disappear.
	 * @param radius collision radius around the center of this object. Value in fraction of a space size.
	 */
	public AsteroidModel(ToroidVector position, Vector velocity, int size, double radius)
	{
		super(position, velocity, radius);
		this.size = size;
	}

	@Override
	/**
	 * Implements a behaviour for an asteroid object.
	 * It updates the position of this asteroid based on how much time has passed since the last update,
	 * which is represented by a parameter deltaTime.
	 */
	public void update(final double deltaTime)
	{
		position = new ToroidVector(position.add(velocity.mul(deltaTime)));
	}
	
	/**
	 * Method that generates an AsteroidMockup object representing this asteroid model object.
	 * @return Returns generated AsteroidMockup object based on the current state of this object
	 */
	@Override
	public AsteroidMockup getMockup()
	{
		return new AsteroidMockup(id, position, radius);
	}
	
	/**
	 * Getter method for a Size of an asteroid. 
	 * @return Returns a size of an asteroid.
	 * This value determines how many times an asteroid will shatter before finally disappearing.
	 */
	public int getSize()
	{
		return size;
	}

	/**
	 * Setter method for a Size of an asteroid. 
	 * This value determines how many times an asteroid will shatter before finally disappearing.
	 * @param size Value being set as a size of an asteroid.
	 */
	public void setSize(final int size)
	{
		if (size <= 0)
		{
			this.size = 0;
		}
		else
		{
			this.size = size;
	
		}
	}
	


	

}
