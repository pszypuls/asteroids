package pszypuls.asteroids.model;

import pszypuls.asteroids.model.mockup.BulletMockup;
import pszypuls.asteroids.util.ToroidVector;
import pszypuls.asteroids.util.Vector;

public class BulletModel extends AbstractModel<BulletMockup>
{

	/**
	 * Remaining lifetime of this bullet in seconds.
	 * When it drops to 0 or below - bullet is considered to be non-existing until it is properly disposed of.
	 */
	private double remainingLifeTime;
	
	
	/**
	 * Public constructor of a bullet model object.
	 * @param remainingLifeTime Specifies how many seconds this bullet still have until it disappears.
	 * @param position Specifies a location of a center of this bullet in a game space.
	 * @param velocity Specifies a velocity vector that this bullet has.
	 * @param radius Specifies a collision radius around the center of this object. Value in fractions of a space size. 
	 */
	public BulletModel(final double remainingLifeTime, final ToroidVector position,
			final Vector velocity, final double radius)
	{
		super(position, velocity, radius);
		this.remainingLifeTime = remainingLifeTime;
	}


	@Override
	/**
	 * Implements a behaviour for an bullet object.
	 * It updates the position of this bullet based on how much time has passed since the last update,
	 * which is represented by a parameter deltaTime.
	 * It also decreases remaining time of life for this bullet based on said parameter.
	 */
	public void update(final double deltaTime)
	{
		remainingLifeTime -= deltaTime;
		position = new ToroidVector(position.add(velocity.mul(deltaTime)));
	}
	
	
	/**
	 * Method that generates an BulletMockup object representing this bullet model object.
	 * @return Returns generated BulletMockup object based on the current state of this object
	 */
	@Override
	public BulletMockup getMockup()
	{
		return new BulletMockup(id, position, radius);
	}
	
	/**
	 * Getter method for a parameter describing how much time a bullet has left before it ceases to exist.
	 * @return Double value refering to remaining lifetime of a bullet
	 */
	public double getRemainingLifeTime()
	{
		return remainingLifeTime;
	}

	/**
	 * Setter method for a parameter describing how much time a bullet has left before it ceases to exist.
	 * @param remainingLifeTime Value that is to be set as a remaining lifetime of a bullet
	 */
	public void setRemainingLifeTime(final double remainingLifeTime)
	{
		this.remainingLifeTime = remainingLifeTime;
	}


	

}
