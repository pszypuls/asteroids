package pszypuls.asteroids.model;

import pszypuls.asteroids.model.mockup.PlayerMockup;
import pszypuls.asteroids.util.EngineStatus;
import pszypuls.asteroids.util.RotationType;
import pszypuls.asteroids.util.ToroidVector;
import pszypuls.asteroids.util.Vector;

public class PlayerModel extends AbstractModel<PlayerMockup> 
{

	/**
	 * Specifies an angle between the X-axis and the direction that this spaceship is currently facing
	 */
	private double angle;
	
	/**
	 * Specifies how fast the spaceship will accelerate due to running engine.
	 */
	private double acceleration;
	
	/**
	 * Enum type representing one of the two possible states that the engine can be in:
	 * RUNNING - engine is currently running
	 * STOPPED - engine is turned off
	 * Specifies whether a ship should accelerate during nearest update or not.
	 */
	private EngineStatus engineStatus;
	
	/**
	 * Rotation speed. Specifies how fast should this spaceship rotate.
	 */
	private double rotationSpeed;
	
	/**
	 * Enum type representing one of the three possible types of rotation:
	 * anticlockwise, clockwise or none. Specifies in what manner should this spaceship rotate during nearest update
	 */
	private RotationType rotationType;
	
	
	/**
	 * Specifies a number of points that this player currently has
	 */
	private int score;
	
	/**
	 * Specifies a number of lives that this player currently has. If this number drops to 0 - game ends.
	 */
	private int lives;

	/**
	 * Specifies time of protection against collisions. When it is positive, the protection is active and player cannot be hit by anything
	 * It is usually set to positive value after being hit by something to give a player some time to find a safe place.
	 */
	private double protectionTime;

	/**
	 * Specifies maximum value for the protectionTime parameter. Is set during a player creation and cannot change afterwards.
	 */
	private final double maxProtectionTime;
	
	/**
	 * Default constructor. Creates a still player with 1 life located at the center of a game space.
	 * It has a collision radius set to 0, so it probably should be set separately to something bigger.
	 */
	public PlayerModel(final double maxProtectionTime)
	{
		super();
		position = new ToroidVector(0.5,0.5);
		angle = 0;
		acceleration = 0;
		engineStatus = EngineStatus.STOPPED;
		rotationType = RotationType.NONE;
		rotationSpeed = 0;
		score = 0;
		lives = 1;
		protectionTime = maxProtectionTime;
		this.maxProtectionTime = maxProtectionTime;
		
	}
	
	@Override
	/**
	 * Implements a behaviour for a player spaceship object.
	 * It updates the position of this spaceship based on how much time has passed since the last update,
	 * which is represented by a parameter deltaTime.
	 * It also calculates new angle of rotation for this spaceship
	 */
	public void update(final double deltaTime)
	{
		angle += rotationType.getValue() * rotationSpeed;
		angle = angle % (2 * Math.PI);
		if (angle < 0 )
		{
			angle += (2 * Math.PI);
		}
		
		Vector direction = getDirection();
		velocity = velocity.add(direction.mul(acceleration*deltaTime*engineStatus.getValue()));
		
		position = new ToroidVector(position.add(velocity.mul(deltaTime)));
		
		protectionTime -= deltaTime;
		if (protectionTime < 0)
		{
			protectionTime = 0;
		}
	}
	
	
	/**
	 * Decreases number of lives that a player has left.
	 */
	public void decrementLives()
	{
		lives -= 1;
		
	}
	
	/**
	 * Method that generates a direction vector based on the angle parameter
	 * @return new instance of a Vector object representing a direction of a spaceship in a 2D space.
	 */
	public Vector getDirection()
	{
		return new Vector(Math.cos(angle), Math.sin(angle));
	}
	
	public PlayerMockup getMockup()
	{
		return new PlayerMockup(id, position, angle, radius, score, lives, protectionTime, engineStatus);
	}


	public double getAngle()
	{
		return angle;
	}


	public void setAngle(final double angle)
	{
		this.angle = angle;
	}


	public double getAcceleration()
	{
		return acceleration;
	}


	public void setAcceleration(final double acceleration)
	{
		this.acceleration = acceleration;
	}


	public EngineStatus getEngineStatus()
	{
		return engineStatus;
	}


	public void setEngineStatus(final EngineStatus engineStatus)
	{
		this.engineStatus = engineStatus;
	}


	public double getRotationSpeed()
	{
		return rotationSpeed;
	}


	public void setRotationSpeed(final double rotationSpeed)
	{
		this.rotationSpeed = rotationSpeed;
	}


	public RotationType getRotationType()
	{
		return rotationType;
	}


	public void setRotationType(final RotationType rotationType)
	{
		this.rotationType = rotationType;
	}


	public int getScore()
	{
		return score;
	}


	public void setScore(final int score)
	{
		this.score = score;
	}

	public void incrementScore(final int points)
	{
		score += points;
	}
	
	public int getLives()
	{
		return lives;
	}


	public void setLives(final int lives)
	{
		this.lives = lives;
	}

	
	public boolean getProtectedStatus()
	{
		return protectionTime <= 0 ? false : true;
	}
	
	public void setProtectedStatus(final boolean protectedStatus)
	{
		if (protectedStatus == true)
		{
			protectionTime = maxProtectionTime;
		}
	}








}
