package pszypuls.asteroids.model.mockup;

import java.util.Map;
import java.util.UUID;

import pszypuls.asteroids.model.GameModel;

/**
 * 
 * @author Patryk Szypulski
 *
 * Mockup of a whole game model. Contains full information about the model that is presented to views.
 * It consists of a mockup object of a player's spaceship and two collections of objects that currently exist in a game model:
 * - collection of asteroids
 * - collection of bullets
 */
public class ModelMockup 
{
	/**
	 * Mockup of a player model
	 */
	public final PlayerMockup playerMockup;
	
	/**
	 * Map of mockups of asteroids currently existing in a game model.
	 * Maps unique identifier of an asteroid object to it's mockup.
	 */
	public final Map<UUID,AsteroidMockup> asteroidMockupMap;
	
	/**
	 * Map of mockups of bullets currently existing in a game model.
	 * Maps unique identifier of a bullet object to it's mockup.
	 */
	public final Map<UUID,BulletMockup> bulletMockupMap;
	
	/**
	 * Public constructor for this class. 
	 * @param gameModel GameModel object for which this mockup is being created.
	 */
	public ModelMockup(final GameModel gameModel)
	{
		playerMockup = gameModel.getPlayerMockup();
		asteroidMockupMap = gameModel.getAsteroidMockupMap();
		bulletMockupMap = gameModel.getBulletMockupMap();
		
		
	}
}
