package pszypuls.asteroids.model.mockup;

import java.util.UUID;

import pszypuls.asteroids.util.ToroidVector;

/**
 * 
 * @author Patryk Szypulski
 *
 * Class that represents a mockup of a bullet model. 
 * It contains whole information from the model necessary for the view to properly handle it.
 */
public class BulletMockup extends AbstractMockup
{
	/**
	 * Unique identifier of an object
	 */
	public final UUID id;
	
	/**
	 * Vertical coordinate of a center of an object. 0.0 is on the left, 1.0 is on the right.
	 */
	public final double positionX;
	
	/**
	 * Horizontal coordinate of a center of an object. 0.0 is on the top, 1.0 is on the bottom.
	 */
	public final double positionY;
	
	/**
	 * Collision radius of an object. Defines a circle that is considered to be an area belonging to this object.
	 */
	public final double radius;
	
	/**
	 * Public constructor of this class.
	 * @param id Unique identifier of a created object
	 * @param position Vector in toroidal space defining  position of a center of this object 
	 * @param radius Double value defining a circle of a given radius around the center of this object
	 */
	public BulletMockup(final UUID id, final ToroidVector position, final double radius)
	{
		this.id = id;
		positionX = position.getX();
		positionY = position.getY();
		this.radius = radius;
	}

}
