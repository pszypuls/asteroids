package pszypuls.asteroids.model.mockup;

/**
 * 
 * @author Patryk Szypulski
 *
 * Abstract parent class for all mockups in the game, except for GameMockup, the top level mockup of a whole game. 
 */
abstract public class AbstractMockup
{
}
