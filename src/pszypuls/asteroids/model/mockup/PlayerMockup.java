package pszypuls.asteroids.model.mockup;

import java.util.UUID;

import pszypuls.asteroids.util.EngineStatus;
import pszypuls.asteroids.util.ToroidVector;

/**
 *
 * Class that represents a mockup of a player model. 
 * It contains whole information from the model necessary for the view to properly handle it.
 * 
 */
public class PlayerMockup extends AbstractMockup
{
	/**
	 * Unique identifier of an object
	 */
	public final UUID id;
	
	/**
	 * Vertical coordinate of a center of an object. 0.0 is on the left, 1.0 is on the right.
	 */
	public final double positionX;
	
	/**
	 * Horizontal coordinate of a center of an object. 0.0 is on the top, 1.0 is on the bottom.
	 */
	public final double positionY;
	
	/**
	 * Collision radius of an object. Defines a circle that is considered to be an area belonging to this object.
	 */
	public final double radius;
	
	/**
	 * Number of lives that this player has left before the game will end.
	 */
	public final int lives;
	
	/**
	 * Accumulated score of this player.
	 */
	public final int score;
	
	/**
	 * Angle in Radians describing rotation of this spaceship relative to vertical axis.
	 */
	public final double angle;
	
	/**
	 * Remaining time when a player is considered to be untouchable.
	 */
	public final double protectionTime;

	/**
	 * Specifies a state of ships engines.
	 */
	public final EngineStatus engineStatus;
	
	
	/**
	 * Public constructor of this class.
	 * @param id Unique identifier of a created object
	 * @param position Vector in toroidal space defining  position of a center of this object 
	 * @param radius Double value defining a circle of a given radius around the center of this object
	 * @param angle Angle in Radians describing rotation of this spaceship relative to vertical axis.
	 * @param score Accumulated score of this player.
	 * @param lives Number of lives that this player has left before the game will end.
	 * @param protectionTime Remaining time when a player is considered to be untouchable.
	 */
	public PlayerMockup(final UUID id, final ToroidVector position, final double angle,
			final double radius, final int score, final int lives, final double protectionTime, final EngineStatus engineStatus)
	{
		this.id = id;
		positionX = position.getX();
		positionY = position.getY();
		this.radius = radius;
		this.score = score;
		this.lives = lives;
		this.angle = angle;
		this.protectionTime = protectionTime;
		this.engineStatus = engineStatus;
	}

}
