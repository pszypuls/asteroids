package pszypuls.asteroids.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.UUID;

import pszypuls.asteroids.model.mockup.AsteroidMockup;
import pszypuls.asteroids.model.mockup.BulletMockup;
import pszypuls.asteroids.model.mockup.PlayerMockup;
import pszypuls.asteroids.util.EngineStatus;
import pszypuls.asteroids.util.GameStatus;
import pszypuls.asteroids.util.RotationType;
import pszypuls.asteroids.util.ToroidVector;
import pszypuls.asteroids.util.Vector;

/**
 * Class representing a model of a whole game. It manages all of the mechanics of the game.
 * 
 * @author Patryk Szypulski
 *
 */
public class GameModel 
{
		
	/**
	 * Specifies radius of a player's spaceship. Radius is used to determine whether collisions occurred or not.
	 * Value in fraction of a space size.
	 */
	private static final double PLAYER_RADIUS = 0.030;

	/**
	 * Specifies acceleration of a player's spaceship due to running engines. That is, how fast spaceship's speed will change.
	 * Value is in fraction of a space size per second squared.
	 */
	private static final double PLAYER_ACCELERATION = 0.15;
	
	/**
	 * Specifies acceleration of a player's spaceship due to running engines. That is, how fast spaceship's speed will change.
	 * Value is in fraction of full rotation per second.
	 */
	private static final double PLAYER_ROTATION_SPEED = 0.07;

	/**
	 * Specifies how long the protected status lasts after is being initialized. 
	 * Value is in seconds.
	 */
	private static final double PROTECTION_TIME = 3;
	
	/**
	 * Specifies how many lives a player has. Each collision with an asteroid takes away one life.
	 */
	private static final int MAX_PLAYER_LIFE = 4;
	
	/**
	 * Specifies the biggest speed the asteroid can have in the game
	 * Value is in fraction of a plane size per second.
	 */
	private static final double MAX_ASTEROID_SPEED = 0.2;

	/**
	 * Specifies how big the asteroid should initially be.
	 * This value determines how many times asteroid can be hit until it finally ceases to shatter into smaller bits and disappears.
	 */
	private static final int MAX_ASTEROID_SIZE = 3;

	/**
	 * Specifies how many (at most) asteroids can be initially generated.
	 * It is used during creation of a game scene to set an upper bound on the number of created asteroids.
	 */
	private static final int MAX_ASTEROID_COUNT = 10;
	
	/**
	 * Specifies radius of an asteroid. Radius is used to determine whether collisions occured or not.
	 * It defines a radius of a biggest asteroid. Radius decreases by a factor of 2 for every decrease in size.
	 * Value in fraction of a space size.
	 */
	private static final double MAX_ASTEROID_RADIUS = 0.08;
	
	/**
	 * Specifies how much points a player will get for each size difference between max size and a size after hit.
	 * Smaller asteroids are thus worth more than the big ones.
	 */
	private static final int POINTS_PER_SIZE = 10;

	/**
	 * Specifies top limit on how many bullets can exist at the same time in a game universe. 
	 */
	private static final int MAX_BULLET_COUNT = 8;

	/**
	 * Specifies how long fired bullets live after being created. Value in seconds.
	 */
	private static final double MAX_BULLET_LIFETIME = 2;
	
	/**
	 * Specifies how fast bullets travel. Value in fraction of a space size per seconds.
	 */
	private static final double BULLET_SPEED = 0.3;

	/**
	 * Specifies radius of a bullet. Value in fraction of a space size.
	 */
	private static final double BULLET_RADIUS = 0.005;
	
	/**
	 * Specifies current status of the game. Can be in one of the four states:
	 * - RUNNING
	 * - PAUSED
	 * - GAME_OVER
	 * - WON
	 */
	private GameStatus gameStatus;
	
	/**
	 * Object representing a player.
	 */
	private final PlayerModel playerModel = new PlayerModel(PROTECTION_TIME); 
	
	/**
	 * Collection of asteroids in the game.
	 */
	private final Collection<AsteroidModel> asteroidCollection = new LinkedList<AsteroidModel>();
	
	/**
	 * Collection of bullets shot by a player.
	 */
	private final Collection<BulletModel> bulletCollection = new LinkedList<BulletModel>();

	
	/**
	 * This method initializes whole game model.
	 * Distributes randomly generated asteroids throughout the game space and initializes a player to be in the center of this space.
	 */
	public void initialize()
	{
		
		gameStatus = GameStatus.RUNNING;
		playerModel.setAngle(0);
		playerModel.setPosition(new ToroidVector(0.5,0.5));
		playerModel.setVelocity(new Vector(0.0,0.0));
		playerModel.setEngineStatus(EngineStatus.STOPPED);
		playerModel.setRadius(PLAYER_RADIUS);
		playerModel.setAcceleration(PLAYER_ACCELERATION);
		playerModel.setRotationSpeed(PLAYER_ROTATION_SPEED);
		playerModel.setRotationType(RotationType.NONE);
		playerModel.setLives(MAX_PLAYER_LIFE);
		playerModel.setScore(0);
		playerModel.setProtectedStatus(true);
		
		System.out.println("Angle: " + playerModel.getAngle());
		
		bulletCollection.clear();
		asteroidCollection.clear();
		for(int i=0; i < MAX_ASTEROID_COUNT; ++i)
		{
			ToroidVector randomPosition;
			do
			{
				randomPosition = new ToroidVector(Math.random(), Math.random());
			}
			while (playerModel.getPosition().distance(randomPosition) < 0.4);
			
			Vector randomVelocity = new Vector((Math.random()-0.5)*MAX_ASTEROID_SPEED, (Math.random()-0.5)*MAX_ASTEROID_SPEED);
			AsteroidModel newAsteroid = new AsteroidModel(randomPosition, randomVelocity, MAX_ASTEROID_SIZE, MAX_ASTEROID_RADIUS);
			asteroidCollection.add(newAsteroid);
		}
		
	}
	
	/**
	 * Updates whole game model progressing it in time by the amount specified by the parameter deltaTime.
	 * Whole logic of a model depending on a passage of time is executed inside this method.
	 * @param Specifies how much time has passed since the last update of the game model. This value is given in seconds.
	 */
	public void update(final double deltaTime)
	{
		if(gameStatus != GameStatus.RUNNING)
		{
			return;
		}
		
		
		playerModel.update(deltaTime);
		
		for(AsteroidModel asteroid : asteroidCollection)
		{
			asteroid.update(deltaTime);
		}
		
		List<BulletModel> removeList = new LinkedList<BulletModel>();
		for(BulletModel bullet : bulletCollection)
		{
			bullet.update(deltaTime);
			
			if (bullet.getRemainingLifeTime() <= 0.0)
			{
				removeList.add(bullet);
			}
		}
		for(BulletModel bullet : removeList)
		{
			bulletCollection.remove(bullet);
		}
		
		
		Map<AsteroidModel, BulletModel> collisionMap = new HashMap<AsteroidModel, BulletModel>();
		
		for(AsteroidModel asteroid : asteroidCollection)
		{
			for(BulletModel bullet : bulletCollection)
			{
				if (asteroid.checkCollision(bullet) == true)
				{
					collisionMap.put(asteroid, bullet);
				}
			}
		}
		for(Entry<AsteroidModel, BulletModel> entry : collisionMap.entrySet())
		{
			asteroidHit(entry.getKey(),entry.getValue());
			if (gameStatus == GameStatus.WON)
			{
				return;
			}
		}
		
		
		if(playerModel.getProtectedStatus() == false)
		{
			AsteroidModel asteroidCollision = null;
			for(AsteroidModel asteroid : asteroidCollection)
			{
				if (playerModel.checkCollision(asteroid) == true)
				{
					asteroidCollision = asteroid;
					break;
				}
			}
			if (asteroidCollision != null)
			{
				playerHit(asteroidCollision);
				rearrange();
			}
			if (playerModel.getLives() == 0)
			{
				setGameStatus(GameStatus.GAME_OVER);
			}
		}
	}
	
	
	/**
	 * Manages the behaviour of a game model when a player wants to fire a bullet.
	 */
	public void bulletFired() 
	{
		if (bulletCollection.size() == MAX_BULLET_COUNT)
		{
			return;
		}
		
		ToroidVector bulletPosition = new ToroidVector(
				playerModel.getPosition().add(playerModel.getDirection().mul(playerModel.getRadius())
				));
		bulletCollection.add(new BulletModel(
				MAX_BULLET_LIFETIME, bulletPosition, playerModel.getDirection().mul(BULLET_SPEED), BULLET_RADIUS
				));
	}
	
	/**
	 * getter method for PlayerMockup
	 * @return Returns PlayerMockup object representing a player  
	 */
	public PlayerMockup getPlayerMockup()
	{
		return playerModel.getMockup();
	}
	
	/**
	 * getter method for AsteroidMockupMap
	 * @return Returns a map of AsteroidMockup objects representing asteroids currently existing in a game model  
	 */
	public Map<UUID,AsteroidMockup> getAsteroidMockupMap()
	{
		Map<UUID,AsteroidMockup> asteroidMockupMap = new TreeMap<UUID,AsteroidMockup>();
		for(AsteroidModel asteroid : asteroidCollection)
		{
			asteroidMockupMap.put(asteroid.getId(), asteroid.getMockup());
		}
		return asteroidMockupMap;
	}
	
	/**
	 * getter method for BulletMockupMap
	 * @return Returns a map of BulletMockup objects representing bullets currently existing in a game model  
	 */
	public Map<UUID,BulletMockup> getBulletMockupMap()
	{
		Map<UUID,BulletMockup> bulletMockupMap = new TreeMap<UUID,BulletMockup>();
		for(BulletModel bullet : bulletCollection)
		{
			bulletMockupMap.put(bullet.getId(),bullet.getMockup());
		}
		return bulletMockupMap;
	}
	
	public void setGameStatus(final GameStatus gameStatus)
	{
		this.gameStatus = gameStatus;
		
	}

	public GameStatus getGameStatus()
	{
		return gameStatus;
		
	}

	public void setPlayerRotationType(final RotationType rotationType)
	{
		playerModel.setRotationType(rotationType);
		
	}

	public void setPlayerEngineStatus(final EngineStatus engineStatus)
	{
		playerModel.setEngineStatus(engineStatus);
		
	}
	
	
	/**
	 * Method used internally to manage the behaviour of a game in case of a player being hit by an asteroid
	 * @param asteroid AsteroidModel object representing the asteroid that hit the player
	 */
	private void playerHit(final AsteroidModel asteroid)
	{
		playerModel.decrementLives();
		playerModel.setProtectedStatus(true);
		
	}

	/**
	 * Method used internally to manage the behaviour of a game in case of an asteroid being hit by a bullet
	 * @param asteroid AsteroidModel object representing the asteroid that was hit
	 * @param bullet BulletModel object representing the bullet that was hit
	 */
	private void asteroidHit(final AsteroidModel asteroid, final BulletModel bullet)
	{
		asteroid.setSize(asteroid.getSize() -1);
		playerModel.incrementScore((MAX_ASTEROID_SIZE - asteroid.getSize())*POINTS_PER_SIZE);
		
		if (asteroid.getSize() > 0)
		{
			for(int i = 0; i < 3; ++i)
			{
				Vector randomDirection = new Vector(Math.random()-0.5, Math.random()-0.5).normalize();
				double randomSpeed = Math.random()*MAX_ASTEROID_SPEED;
				ToroidVector newPosition = new ToroidVector( randomDirection.mul( asteroid.getRadius()/2 ).add( asteroid.getPosition() ) );
				asteroidCollection.add(new AsteroidModel(newPosition, randomDirection.mul(randomSpeed), asteroid.getSize(), asteroid.getRadius()/2));
			}
		}
		asteroidCollection.remove(asteroid);
		bulletCollection.remove(bullet);
		
		if (asteroidCollection.size() == 0)
		{
			gameStatus = GameStatus.WON;
		}
	}

	
	
	/**
	 * Rearranges a scene after collision with an asteroid.
	 */
	private void rearrange()
	{
		playerModel.setPosition(new ToroidVector(0.5,0.5));
		playerModel.setVelocity(new Vector(0,0));
		playerModel.setAngle(0);
		
		for(AsteroidModel asteroid : asteroidCollection)
		{
			ToroidVector randomPosition;
			do
			{
				randomPosition = new ToroidVector(Math.random(), Math.random());
			}
			while (playerModel.getPosition().distance(randomPosition) < 0.2);
			
			Vector randomVelocity = new Vector((Math.random()-0.5)*MAX_ASTEROID_SPEED, (Math.random()-0.5)*MAX_ASTEROID_SPEED);
			
			asteroid.setPosition(randomPosition);
			asteroid.setVelocity(randomVelocity);
		}
		
		bulletCollection.clear();
	}
}
