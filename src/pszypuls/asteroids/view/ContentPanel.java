package pszypuls.asteroids.view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Map;
import java.util.UUID;

import javax.swing.JPanel;

/**
 * A class extending JPanel. It functions as a canvas on which every visible element of the game is drawn.
 * @author Patryk Szypulski
 *
 */
public class ContentPanel extends JPanel
{
	private static final long serialVersionUID = 1L;
	
	
	/**
	 * Object representing a player's spaceship in a window. Contains information and method necessary to draw it properly on screen
	 */
	private final PlayerView playerView;
	
	/**
	 * Collection of objects representing an asteroid in a window. Contains information and method necessary to draw it properly on screen
	 */
	private final Map<UUID,AsteroidView> asteroidViewMap;
	
	/**
	 * Collection of objects representing a bullet in a window. Contains information and method necessary to draw it properly on screen
	 */
	private final Map<UUID,BulletView> bulletViewMap;
	
	public ContentPanel(final PlayerView playerView, final Map<UUID,AsteroidView> asteroidViewMap, final Map<UUID,BulletView> bulletViewMap)
	{
		this.playerView = playerView;
		this.asteroidViewMap = asteroidViewMap;
		this.bulletViewMap = bulletViewMap;
	}
	
	/**
	 * Overridden method defining how this panel is going to be painted
	 */
	@Override
	protected void paintComponent(Graphics g)
	{
		/**
		 * TODO
		 */
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.BLACK);
		setBackground(Color.BLACK);
		
		playerView.draw(g2d, getWidth(), getHeight());
		
		for(AsteroidView asteroidView : asteroidViewMap.values())
		{
			asteroidView.draw(g2d, getWidth(), getHeight());
		}
		for(BulletView bulletView : bulletViewMap.values())
		{
			bulletView.draw(g2d, getWidth(), getHeight());
		}
	}
}
