package pszypuls.asteroids.view;

import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import pszypuls.asteroids.message.AbstractMessage;
import pszypuls.asteroids.model.mockup.AsteroidMockup;
import pszypuls.asteroids.model.mockup.BulletMockup;

/**
 * A view inherited from AbstractView. Represents a top-level View component of the game.
 * It contains all of the other view-related elements as well as a main draw loop.
 * 
 * @author Patryk Szypulski
 */
public class SwingView extends AbstractView implements Runnable, ComponentListener
{
	/**
	 * Upper bound on number of frames that are to be rendered per second.
	 */
	private static final int FRAMES_PER_SECOND = 50;

	/**
	 * Swing window of a game.
	 */
	private final JFrame frame;
	
	/**
	 * Panel containing whole visible content of a game.
	 */
	private final ContentPanel contentPanel;
	
	private final Keyboard keyboard;
	
	/**
	 * Object representing a player's spaceship in a window. Contains information and method necessary to draw it properly on screen
	 */
	private final PlayerView playerView = new PlayerView();
	
	/**
	 * Collection of objects representing an asteroid in a window. Contains information and method necessary to draw it properly on screen
	 */
	private final Map<UUID,AsteroidView> asteroidViewMap = new TreeMap<UUID,AsteroidView>();
	
	/**
	 * Collection of objects representing a bullet in a window. Contains information and method necessary to draw it properly on screen
	 */
	private final Map<UUID,BulletView> bulletViewMap = new TreeMap<UUID,BulletView>();
	
	/**
	 * Thread-safe queue of messages shared between view and controller.
	 */
	@SuppressWarnings("unused")
	private final BlockingQueue<AbstractMessage> messageQueue;
	
	
	/**
	 * Constructor for this view.
	 * 
	 * @param title Title of a game window
	 * @param width Window width in pixels
	 * @param height Window height in pixels
	 */
	public SwingView(final String title, final int width, final int height, final BlockingQueue<AbstractMessage> messageQueue) 
	{
		this.messageQueue = messageQueue;
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		keyboard = new Keyboard(messageQueue);
		frame.addKeyListener(keyboard);
		
		frame.setTitle(title);
		
		frame.setSize(width, height);
		frame.setExtendedState(JFrame.MAXIMIZED_VERT);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.addComponentListener(this);
		
		contentPanel = new ContentPanel(playerView, asteroidViewMap, bulletViewMap);
		frame.add(contentPanel);
		
	}
	
	/**
	 * Method that is automatically run when a thread starts.
	 * This method contains main rendering loop.
	 */
	public void run() 
	{
		while (true)
		{
			swapMockups();
			
			updateViews();
			
			
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					frame.setTitle("Score: "+ currentMockup.playerMockup.score +", Lives: "+ currentMockup.playerMockup.lives);
					contentPanel.paintImmediately(0, 0, frame.getWidth(), frame.getHeight());
				}
			});
			frame.setVisible(true);
			
			try {
				Thread.sleep(1000/FRAMES_PER_SECOND);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
	/**
	 * Updates, adds and removes view objects associated with player, asteroids and bullets in the game 
	 * based on a currently available mockup object.
	 */
	private void updateViews()
	{
		updatePlayerView();
		
		List<UUID> removeList = new LinkedList<UUID>();
		for(UUID id : asteroidViewMap.keySet())
		{
			AsteroidMockup asteroidMockup = currentMockup.asteroidMockupMap.get(id);
			if( asteroidMockup != null)
			{
				updateAsteroidView(id, asteroidMockup);
			}
			else
			{
				removeList.add(id);
			}
		}
		for (UUID id : removeList)
		{
			asteroidViewMap.remove(id);
		}
		
		for(UUID id : currentMockup.asteroidMockupMap.keySet())
		{
			
			if (asteroidViewMap.containsKey(id) == false)
			{
				AsteroidMockup asteroidMockup = currentMockup.asteroidMockupMap.get(id);
				AsteroidView asteroidView = new AsteroidView(asteroidMockup.positionX, asteroidMockup.positionY, asteroidMockup.radius);
				asteroidViewMap.put(id, asteroidView);
			}
		}
		
		removeList = new LinkedList<UUID>();
		for(UUID id : bulletViewMap.keySet())
		{
			BulletMockup bulletMockup = currentMockup.bulletMockupMap.get(id);
			if( bulletMockup != null)
			{
				updateBulletView(id, bulletMockup);
			}
			else
			{
				removeList.add(id);
			}
		}
		for (UUID id : removeList)
		{
			bulletViewMap.remove(id);
		}
		
		
		for(UUID id : currentMockup.bulletMockupMap.keySet())
		{
			
			if (bulletViewMap.containsKey(id) == false)
			{
				BulletMockup bulletMockup = currentMockup.bulletMockupMap.get(id);
				BulletView bulletView = new BulletView(bulletMockup.positionX, bulletMockup.positionY, bulletMockup.radius);
				bulletViewMap.put(id, bulletView);
			}
		}
	}
	
	/**
	 * Updates attributes of a PlayerView object
	 */
	private void updatePlayerView()
	{
		playerView.setPositionX(currentMockup.playerMockup.positionX);
		playerView.setPositionY(currentMockup.playerMockup.positionY);
		playerView.setAngle(currentMockup.playerMockup.angle);
		playerView.setRadius(currentMockup.playerMockup.radius);
		playerView.setProtectionTime(currentMockup.playerMockup.protectionTime);
		playerView.setEngineStatus(currentMockup.playerMockup.engineStatus);
		
	}
	/**
	 * Updates attributes of a existing BulletView objects
	 */
	private void updateBulletView(final UUID id, final BulletMockup bulletMockup)
	{
		BulletView bulletView = bulletViewMap.get(id);
		bulletView.setPositionX(bulletMockup.positionX);
		bulletView.setPositionY(bulletMockup.positionY);
		bulletView.setRadius(bulletMockup.radius);
		
	}
	
	/**
	 * Updates attributes of a existing AsteroidView objects
	 */
	private void updateAsteroidView(final UUID id, final AsteroidMockup asteroidMockup)
	{
		AsteroidView asteroidView = asteroidViewMap.get(id);
		asteroidView.setPositionX(asteroidMockup.positionX);
		asteroidView.setPositionY(asteroidMockup.positionY);
		asteroidView.setRadius(asteroidMockup.radius);
	}
	
	
	@Override
	public void componentResized(ComponentEvent arg0) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				frame.setSize(frame.getHeight(), frame.getHeight());
				frame.setLocationRelativeTo(null);
			}
		});
	}

	@Override
	public void componentMoved(ComponentEvent e)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void componentShown(ComponentEvent e)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void componentHidden(ComponentEvent e)
	{
		// TODO Auto-generated method stub
		
	}

}
