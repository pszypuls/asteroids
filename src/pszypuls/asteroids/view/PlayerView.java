package pszypuls.asteroids.view;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;

import pszypuls.asteroids.util.EngineStatus;

public class PlayerView
{
	/**
	 * Vertical coordinate of a center of an object. 0.0 is on the left, 1.0 is on the right.
	 */
	private double positionX;
	
	/**
	 * Horizontal coordinate of a center of an object. 0.0 is on the top, 1.0 is on the bottom.
	 */
	private double positionY;
	
	/**
	 * Collision radius of an object. Defines a circle that is considered to be an area belonging to this object.
	 */
	private double radius;
	
	/**
	 * Angle in Radians describing rotation of this spaceship relative to vertical axis.
	 */
	private double angle;
	
	/**
	 * Remaining time when a player is considered to be untouchable.
	 */
	private double protectionTime;
	
	/**
	 * Specifies a state of ships engines.
	 */
	EngineStatus engineStatus;
	
	/**
	 * Points describing a triangle used to draw this player's ship
	 */
	private final double[] xpoints = {-0.55,0.55,-0.55};
	/**
	 * Points describing a triangle used to draw this player's ship
	 */
	private final double[] ypoints = {-0.5,0,0.5};
	/**
	 * Number of points in a polygon used to draw this player's ship
	 */
	private final int npoints = 3;
	
	
	/**
	 * Default constructor of this class.
	 */
	public PlayerView()
	{
		this.positionX = 0;
		this.positionY = 0.0;
		this.angle = 0;
		this.radius = 0.0;
	}
	
	/**
	 * Public constructor of this class.
	 * @param positionX Vertical coordinate of a center of an object. 0.0 is on the left, 1.0 is on the right.
	 * @param positionY Horizontal coordinate of a center of an object. 0.0 is on the top, 1.0 is on the bottom.
	 * @param radius Collision radius of an object. Defines a circle that is considered to be an area belonging to this object.
	 * @param protectionTime Remaining time when a player is considered to be untouchable.
	 */
	public PlayerView(final double positionX, final double positionY, final double angle, final double radius, final double protectionTime)
	{
		this.positionX = positionX;
		this.positionY = positionY;
		this.angle = angle;
		this.radius = radius;
		this.protectionTime = protectionTime;
	}
	
	/**
	 * Method managing all the action revolving around drawing this asteroid on a screen.
	 * @param g2d Graphics2D object used in drawing this asteroid
	 * @param width Width of a screen in pixels
	 * @param height Height of a screen in pixels
	 */
	public void draw(Graphics2D g2d, int width, int height)
	{
		int[] widthPoints = new int[npoints];
		int[] heightPoints = new int[npoints];

		
		for (int i = 0; i < npoints; ++i)
		{
			widthPoints[i] = (int) (radius*xpoints[i]*width);
			heightPoints[i] = (int) (radius*ypoints[i]*height);
		}
		g2d.setColor(Color.WHITE);
		g2d.translate(width*positionX, height*positionY);
		g2d.rotate(angle);
		
		if (engineStatus == EngineStatus.RUNNING)
		{
			int[] smallFireWidthPoints = new int[npoints];
			int[] smallFireUpHeightPoints = new int[npoints];
			int[] smallFireDownHeightPoints = new int[npoints];
			int[] bigFireWidthPoints = new int[npoints];
			int[] bigFireHeightPoints = new int[npoints];
			for (int i = 0; i < npoints; ++i)
			{
				
				smallFireWidthPoints[i] = (int) ((-0.65-0.25*xpoints[i])*radius*width);
				smallFireUpHeightPoints[i] = (int) ((-0.25-0.25*ypoints[i])*radius*width);
				smallFireDownHeightPoints[i] = (int) ((0.25-0.25*ypoints[i])*radius*width);
				bigFireWidthPoints[i] = (int) ((-0.75-0.5*xpoints[i])*radius*width);
				bigFireHeightPoints[i] = (int) (0.5*ypoints[i]*radius*width);
			}
			g2d.fill(new Polygon(smallFireWidthPoints, smallFireUpHeightPoints, npoints));
			g2d.fill(new Polygon(smallFireWidthPoints, smallFireDownHeightPoints, npoints));
			g2d.fill(new Polygon(bigFireWidthPoints, bigFireHeightPoints, npoints));
		}
		
		if (protectionTime == 0 || (protectionTime % 1) > 0.5)
		{
			g2d.draw(new Polygon(widthPoints,heightPoints,npoints));
		}
		else
		{
			g2d.fill(new Polygon(widthPoints,heightPoints,npoints));
		}
		
		g2d.rotate(-angle);
		g2d.translate(-width*positionX,-height*positionY);
		
	}
	public double getPositionX()
	{
		return positionX;
	}
	public void setPositionX(final double positionX)
	{
		this.positionX = positionX;
	}
	public double getPositionY()
	{
		return positionY;
	}
	public void setPositionY(final double positionY)
	{
		this.positionY = positionY;
	}
	public double getAngle()
	{
		return angle;
	}
	public void setAngle(final double angle)
	{
		this.angle = angle;
	}
	public double getRadius()
	{
		return radius;
	}
	public void setRadius(final double radius)
	{
		this.radius = radius;
	}	
	public double getProtectionTime()
	{
		return protectionTime;
	}
	public void setProtectionTime(final double protectionTime)
	{
		this.protectionTime = protectionTime;
	}

	EngineStatus getEngineStatus()
	{
		return engineStatus;
	}
	public void setEngineStatus(EngineStatus engineStatus)
	{
		this.engineStatus = engineStatus;
		
	}
}
