package pszypuls.asteroids.view;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.concurrent.BlockingQueue;

import pszypuls.asteroids.message.AbstractMessage;
import pszypuls.asteroids.message.BulletFiredMessage;
import pszypuls.asteroids.message.EngineChangeMessage;
import pszypuls.asteroids.message.PauseStateChangeMessage;
import pszypuls.asteroids.message.RestartMessage;
import pszypuls.asteroids.message.RotationChangeMessage;
import pszypuls.asteroids.util.EngineStatus;
import pszypuls.asteroids.util.RotationType;

/**
 * Class that manages any behaviour connected with keyboard inputs.
 * @author patryk
 *
 */
public class Keyboard implements KeyListener 
{
	/**
	 * blocking queue that transmits information about any occuring events to the controller.	
	 */
	private BlockingQueue<AbstractMessage> messageQueue;
	
	/**
	 * Pressing this key will speed up the player's ship in the currently pointing direction
	 */
	private final static int ENGINE_RUNNING_KEY = KeyEvent.VK_UP;
	
	/**
	 * Pressing this key will rotate the spaceship anticlockwise. 
	 */
	private final static int ROTATE_ANTICLOCKWISE_KEY = KeyEvent.VK_LEFT;
	
	/**
	 * Pressing this key will rotate the spaceship clockwise. 
	 */
	private final static int ROTATE_CLOCKWISE_KEY = KeyEvent.VK_RIGHT;
	
	/**
	 * Pressing this key will fire bullet in the direction currently pointed by the player's ship
	 */
	private final static int FIRE_BULLET_KEY = KeyEvent.VK_SPACE;
	
	/**
	 * Pressing this key will restart the game.
	 */
	private final static int RESTART_KEY = KeyEvent.VK_ENTER;
	
	/**
	 * Pressing this key will pause/unpause game.
	 */
	private final static int PAUSE_KEY = KeyEvent.VK_ESCAPE;
	
	
	/**
	 * Default constructor. Initializes pressedKeyMap to 256-size array.
	 */
	public Keyboard(BlockingQueue<AbstractMessage> messageQueue)
	{
		this.messageQueue = messageQueue;
	}
	
	@Override
	public void keyPressed(KeyEvent keyEvent) 
	{
		switch(keyEvent.getKeyCode())
		{
			case FIRE_BULLET_KEY :
				try
				{
					messageQueue.put(new BulletFiredMessage());
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				};
				break;
			case ROTATE_ANTICLOCKWISE_KEY :
				try
				{
					messageQueue.put(new RotationChangeMessage(RotationType.ANTICLOCKWISE));
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				};
				break;
			case ROTATE_CLOCKWISE_KEY :
				try
				{
					messageQueue.put(new RotationChangeMessage(RotationType.CLOCKWISE));
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				};
				break;
			case ENGINE_RUNNING_KEY :
				try
				{
					messageQueue.put(new EngineChangeMessage(EngineStatus.RUNNING));
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				};
				break;
			case PAUSE_KEY :
				try
				{
					messageQueue.put(new PauseStateChangeMessage());
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				};
				break;
			case RESTART_KEY :
				try
				{
					messageQueue.put(new RestartMessage());
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				};
				break;
		}
	}
  
	@Override
	public void keyReleased(KeyEvent keyEvent) 
	{
		switch(keyEvent.getKeyCode())
		{
			case KeyEvent.VK_LEFT :
				try
				{
					messageQueue.put(new RotationChangeMessage(RotationType.NONE));
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				};
				break;
			case KeyEvent.VK_RIGHT :
				try
				{
					messageQueue.put(new RotationChangeMessage(RotationType.NONE));
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				};
				break;
			case KeyEvent.VK_UP :
				try
				{
					messageQueue.put(new EngineChangeMessage(EngineStatus.STOPPED));
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				};
				break;
		}
	}

	@Override
	public void keyTyped(KeyEvent keyEvent) 
	{
		
	}

}
