package pszypuls.asteroids.view;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;

/**
 * Class combining all information an methods of drawing of a particular asteroid in a SwingView.
 * @author patryk
 *
 */
public class AsteroidView
{
	/**
	 * Vertical coordinate of a center of an object. 0.0 is on the left, 1.0 is on the right.
	 */
	private double positionX;
	
	/**
	 * Horizontal coordinate of a center of an object. 0.0 is on the top, 1.0 is on the bottom.
	 */
	private double positionY;
	
	/**
	 * Collision radius of an object. Defines a circle that is considered to be an area belonging to this object.
	 */
	private double radius;
	
	/**
	 * Public constructor of this class.
	 * @param positionX Vertical coordinate of a center of an object. 0.0 is on the left, 1.0 is on the right.
	 * @param positionY Horizontal coordinate of a center of an object. 0.0 is on the top, 1.0 is on the bottom.
	 * @param radius Collision radius of an object. Defines a circle that is considered to be an area belonging to this object.
	 */
	public AsteroidView(final double positionX, final double positionY, final double radius)
	{
		this.positionX = positionX;
		this.positionY = positionY;
		this.radius = radius;
	}
	
	/**
	 * Method managing all the action revolving around drawing this asteroid on a screen.
	 * @param g2d Graphics2D object used in drawing this asteroid
	 * @param width Width of a screen in pixels
	 * @param height Height of a screen in pixels
	 */
	public void draw(Graphics2D g2d, int width, int height)
	{
		g2d.setColor(Color.WHITE);
		g2d.translate(-width*radius/2, -width*radius/2);
		g2d.draw(new Ellipse2D.Double(positionX*width, positionY*height, radius*width, radius*height));
		g2d.translate(width*radius/2,width*radius/2);
	}
	
	
	public double getPositionX()
	{
		return positionX;
	}
	public void setPositionX(double positionX)
	{
		this.positionX = positionX;
	}
	public double getPositionY()
	{
		return positionY;
	}
	public void setPositionY(double positionY)
	{
		this.positionY = positionY;
	}
	public double getRadius()
	{
		return radius;
	}
	public void setRadius(double radius)
	{
		this.radius = radius;
	}
}
