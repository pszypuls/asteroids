package pszypuls.asteroids.view;

import pszypuls.asteroids.model.mockup.ModelMockup;

/**
 * Abstract parent class for all views. All other top-level views must inherit from this class.
 * Supplies views with a thread-safe mechanism for setting and swapping mockups of a game model.
 * 
 * @author Patryk Szypulski
 */
abstract public class AbstractView 
{
	/**
	 * Buffered mockup of a game model
	 * Will be used in next frame rendering
	 */
	private ModelMockup bufferedMockup;

	/**
	 * Currently used mockup of a game model
	 */
	protected ModelMockup currentMockup;
	
	/**
	 * Setter method for buffered mockup
	 * This method is thread-safe.
	 */
	synchronized public void setBufferedMockup(final ModelMockup bufferedMockup) 
	{
		this.bufferedMockup = bufferedMockup;
	}
	/**
	 *  Swaps buffered mockup with current one. Currently used mockup is lost.
	 *  This method is thread-safe.
	 */
	synchronized public void swapMockups() 
	{
		if (bufferedMockup != null)
		{
			currentMockup = bufferedMockup;
			bufferedMockup = null;
		}
	}
	/**
	 * getter method for currently used model Mmckup.
	 * 
	 * @return Returns ModelMockup object representing currently used mockup of a game model
	 */
	public ModelMockup getCurrentMockup()
	{
		return currentMockup;
	}
}	
