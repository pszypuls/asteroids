package pszypuls.asteroids;

import pszypuls.asteroids.controller.AsteroidsController;
/**
 * Remake of a classic Asteroids game.  
 * @version 0.8
 * @author Patryk Szypulski
 *
 */
public class Main
{

	/**
	 * Entry point of this game application. 
	 * @param args arguments passed to game app from console. Currently not used.
	 */
	public static void main(String[] args)
	{
		Thread gameThread = new Thread(new AsteroidsController());
		gameThread.run();
	}
}
