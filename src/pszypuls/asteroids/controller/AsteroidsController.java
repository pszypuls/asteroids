package pszypuls.asteroids.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import pszypuls.asteroids.message.AbstractMessage;
import pszypuls.asteroids.message.BulletFiredMessage;
import pszypuls.asteroids.message.EngineChangeMessage;
import pszypuls.asteroids.message.PauseStateChangeMessage;
import pszypuls.asteroids.message.RestartMessage;
import pszypuls.asteroids.message.RotationChangeMessage;
import pszypuls.asteroids.model.GameModel;
import pszypuls.asteroids.model.mockup.ModelMockup;
import pszypuls.asteroids.util.EngineStatus;
import pszypuls.asteroids.util.GameStatus;
import pszypuls.asteroids.util.RotationType;
import pszypuls.asteroids.view.AbstractView;
import pszypuls.asteroids.view.SwingView;

public class AsteroidsController implements Runnable
{
	/**
	 * Specifies how many updates (at most) will be made to game model per second.
	 */
	private static final double UPDATES_PER_SECOND = 50;
	/**
	 * View of this application.
	 */
	private  static AbstractView view;
	/**
	 * Thread running the view of this game.
	 */
	private  static Thread viewThread;
	
	/**
	 * Object containing a whole game logic. It manages all objects' behaviour and interactions.
	 */
	private final static GameModel gameModel = new GameModel();
	
	/**
	 * A synchronized queue used as a sole communication channel between view and a controller. 
	 * It allows for information about events in view to be delivered to this controller.
	 */
	private static final BlockingQueue<AbstractMessage> messageQueue = new LinkedBlockingQueue<AbstractMessage>();
	
	/**
	 * Map thats associates specific type of message inherited from AbstractMessage with an apropriate handler for this type of message.
	 */
	private static final Map<Class<? extends AbstractMessage>, MessageHandler> messageHandlerMap = new HashMap<Class<? extends AbstractMessage>, MessageHandler>();
	
	/**
	 * Method that initializes all elements of the game before main loop starts.
	 */
	private void init()
	{
				
		mapMessageHandlers();
		view = new SwingView("Asteroids", 1000, 1000, messageQueue);
		viewThread = new Thread( (SwingView) view);		
		gameModel.initialize();
		view.setBufferedMockup(new ModelMockup(gameModel));
	}

	/**
	 * Method thats fills messageHandlerMap with appropriate connections between a specific message classes and their handlers.
	 */
	private void mapMessageHandlers()
	{
		messageHandlerMap.put(RotationChangeMessage.class, new RotationChangeMessageHandler());
		messageHandlerMap.put(EngineChangeMessage.class, new EngineChangeMessageHandler());
		messageHandlerMap.put(BulletFiredMessage.class, new BulletFiredMessageHandler());
		messageHandlerMap.put(PauseStateChangeMessage.class, new PauseStateChangeMessageHandler());
		messageHandlerMap.put(RestartMessage.class, new RestartMessageHandler());
	}


	/**
	 * Method that is executed in a separate thread after this object is created.
	 * Contains main loop of the game.
	 */
	public void run()
	{
		init();
		viewThread.start();
		
		//time between model updates in seconds
		double deltaTime = (double) 1.0/UPDATES_PER_SECOND;
		
		
		// convert the time to seconds
        double nextTime = (double)System.nanoTime() / 1000000000.0;
   
        
        while(true)
        {
            // convert the time to seconds
            double currTime = (double)System.nanoTime() / 1000000000.0;
            

            
            if(gameModel.getGameStatus() == GameStatus.RUNNING && currTime >= nextTime)
            {
                // assign the time for the next update
                nextTime += deltaTime;
                
                gameModel.update(deltaTime);
                      
                view.setBufferedMockup(new ModelMockup(gameModel));          
            }
            else
            {
                // calculate the time left till another update
                double waitTime = nextTime - currTime;
                
                while(waitTime > 0 || gameModel.getGameStatus() != GameStatus.RUNNING)
                {
                	AbstractMessage message = messageQueue.poll();
                	if (message != null)
                	{
                		process(message);
                	}
					currTime = (double)System.nanoTime() / 1000000000.0;
					waitTime = nextTime - currTime;
					if (waitTime < 0)
					{
						nextTime = currTime;
					}
                }
            }
        }
		
	}

	/**
	 * Method used to process messages coming from the view.
	 * It uses a map that associates types of messages with the functions that handle them.
	 * @param message that is being 
	 */
	private void process(final AbstractMessage message)
	{
		MessageHandler messageHandler = messageHandlerMap.get(message.getClass());
		if (messageHandler != null)
		{
			messageHandler.process(message);
		}
		
	}
	
	
	/**
	 * Interface describing an object responsible for processing a message extended from an AbstractMessage.
	 * All message handlers must implement this interface
	 */
	private interface MessageHandler
	{
		abstract public void process(final AbstractMessage message);

	}
	
	/**
	 * Implementation of a message handler interface.
	 * It is responsible for processing a message about change in type of rotation a player's ship is experiencing.
	 */
	private class RotationChangeMessageHandler implements MessageHandler
	{
		@Override
		public void process(final AbstractMessage message)
		{
			if (gameModel.getGameStatus() == GameStatus.RUNNING)
			{
				RotationChangeMessage rotationMessage = (RotationChangeMessage) message;
				gameModel.setPlayerRotationType(rotationMessage.getRotationType());
			}
		}

	}
	
	/**
	 * Implementation of a message handler interface.
	 * It is responsible for processing a message about change in a status of a player ship's engine.
	 */
	private class EngineChangeMessageHandler implements MessageHandler
	{
		@Override
		public void process(AbstractMessage message)
		{
			if (gameModel.getGameStatus() == GameStatus.RUNNING)
			{
				EngineChangeMessage engineMessage = (EngineChangeMessage) message;
				gameModel.setPlayerEngineStatus(engineMessage.getEngineStatus());
			}
		}

	}
	
	/**
	 * Implementation of a message handler interface.
	 * It is responsible for processing a message about firing a bullet from a player's ship.
	 */
	private class BulletFiredMessageHandler implements MessageHandler
	{
		@Override
		public void process(AbstractMessage message)
		{
			if (gameModel.getGameStatus() == GameStatus.RUNNING)
			{
				gameModel.bulletFired();
			}
		}
		
	}
	
	/**
	 * Implementation of a message handler interface.
	 * It is responsible for processing a message about a change in game status to or from a paused state.
	 */
	private class PauseStateChangeMessageHandler implements MessageHandler
	{
		@Override
		public void process(AbstractMessage message)
		{
			if (gameModel.getGameStatus() == GameStatus.PAUSED)
			{
				gameModel.setPlayerEngineStatus(EngineStatus.STOPPED);
				gameModel.setPlayerRotationType(RotationType.NONE);
				gameModel.setGameStatus(GameStatus.RUNNING);
			}
			else if (gameModel.getGameStatus() == GameStatus.RUNNING)
			{
				gameModel.setGameStatus(GameStatus.PAUSED);
			}
		}
	}
	
	/**
	 * Implementation of a message handler interface.
	 * It is responsible for processing a message about a request for a game restart.
	 */
	private class RestartMessageHandler implements MessageHandler
	{
		@Override
		public void process(AbstractMessage message)
		{
			gameModel.initialize();
		}
	}
}
