package pszypuls.asteroids.message;

/**
 * Abstract parent class for all messages that are being sent from a view to controller.
 * @author Patryk Szypulski
 *
 */
public abstract class AbstractMessage 
{
}
