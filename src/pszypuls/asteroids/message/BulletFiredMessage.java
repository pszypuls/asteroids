package pszypuls.asteroids.message;

/**
 * Message describing an event of a bullet being fired from a player's ship
 * @author Patryk Szypulski
 *
 */
public class BulletFiredMessage extends AbstractMessage
{
	
}
