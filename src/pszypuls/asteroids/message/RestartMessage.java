package pszypuls.asteroids.message;

/**
 * Message describing a request for restarting a game.
 * @author Patryk Szypulski
 *
 */
public class RestartMessage extends AbstractMessage
{

}
