package pszypuls.asteroids.message;

import pszypuls.asteroids.util.EngineStatus;

/**
 * Message describing a change in player ship's engine status.
 * @author Patryk Szypulski
 *
 */
public class EngineChangeMessage extends AbstractMessage
{

	private EngineStatus engineStatus;
	
	public EngineChangeMessage(EngineStatus engineStatus)
	{
		this.engineStatus = engineStatus;
	}
	
	public EngineStatus getEngineStatus()
	{
		return engineStatus;
	}
}
