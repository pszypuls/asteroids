package pszypuls.asteroids.message;


/**
 * Message describing a change in game status to or from paused state.
 * @author Patryk Szypulski
 *
 */
public class PauseStateChangeMessage extends AbstractMessage
{
	
}
