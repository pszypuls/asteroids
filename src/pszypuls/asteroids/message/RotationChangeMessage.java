package pszypuls.asteroids.message;

import pszypuls.asteroids.util.RotationType;

/**
 * Message describing a change in a player's ship rotation type.
 * @author Patryk Szypulski
 *
 */
public class RotationChangeMessage extends AbstractMessage
{

	private RotationType rotationType;
	
	public RotationChangeMessage(RotationType rotationType)
	{
		this.rotationType = rotationType;
	}
	
	public RotationType getRotationType()
	{
		return rotationType;
	}
}
