package pszypuls.asteroids.util;

/**
 * Enum type describing possible states of a player ship's engine.
 * ANTICLOCKWISE - a ship is rotating in an anticlockwise manner.
 * CLOCKWISE - a ship is rotating in a clockwise manner.
 * NONE - ship is not rotating.
 * @author patryk
 *
 */
public enum RotationType
{
	ANTICLOCKWISE(-1), NONE(0), CLOCKWISE(1);
	private int value;
	
	private RotationType(final int value)
	{
		this.value = value;
	}
	public int getValue()
	{
		return value;
	}
};
