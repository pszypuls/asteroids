package pszypuls.asteroids.util;

/**
 * Enum type describing possible states that the game can be in.
 * RUNNING - game is currently running
 * PAUSED - game is paused. Time does not flow during a paused state and only system-type messages are handled.
 * GAME_OVER - game has ended. Losing condition has occurred.
 * WON - game has ended. Winning condition has occurred.
 */
public enum GameStatus
{
	RUNNING, PAUSED, GAME_OVER, WON;
}
