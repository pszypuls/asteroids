package pszypuls.asteroids.util;



/**
 * Class representing a point or a vector in linear space.
 * Parameters X and Y are of type double.
 * 
 * @author Patryk Szypulski
 *
 */
public class Vector {
	/**
	 * Diagonal coordinate of a vector.
	 */
	protected double x;
	
	/**
	 * Horizontal coordinate of a vector.
	 */
	protected double y; 
	
	/**
	 * Default constructor 
	 */
	public Vector()
	{
		this.x = 0;
		this.y = 0;
	}
	
	/**
	 * Constructor for a vector class.
	 * @param x horizontal coordinate of a vector
	 * @param y vertical coordinate of a vector
	 */
	public Vector(final double x, final double y) 
	{
		this.setX(x);
		this.setY(y);
	}


	/**
	 * Adds two vectors and returns result as new Vector object
	 * @param vector vector being added to the one that runs this method.
	 * @return Returns new instance of a vector class that is a sum of the two.
	 */
	public Vector add(final Vector vector)
	{
		Vector result = new Vector();
		result.setX(this.x+vector.getX());
		result.setY(this.y+vector.getY());
		return result;
	}
	
	/**
	 * Subtracts two vectors and returns result as new Vector object
	 * @param vector vector being subtracted from the one that runs this method.
	 * @return Returns result as a new instance of a vector class.
	 */
	public Vector sub(final Vector vector)
	{
		Vector result = new Vector();
		result.setX(this.x-vector.getX());
		result.setY(this.y-vector.getY());
		return result;
	}	
	
	/**
	 * Multiplies a vector by a scalar given as a parameter
	 * @param scalar Double value scalar that multiplies
	 * @return Returns result as a new instance of vector class 
	 */
	public Vector mul(final double scalar)
	{
		return new Vector(scalar*this.getX(), scalar*this.getY());
	}
	
	
	/**
	 * Dot product of two vectors
	 * @param vector one of the arguments of a dot product.
	 * @return Returns a double value of a dot product of this vector and a vector given as a parameter 
	 */
	public double dot(final Vector vector)
	{
		return this.x*vector.getX() + this.y*vector.getY();
	}
	
	/**
	 * Returns a length of a given vector
	 * @return
	 */
	public double length()
	{
		return Math.sqrt(this.dot(this));
	}
	
	/**
	 * Normalizes a vector so that it's value is equal to one.
	 * Does not change the object for which this method is run.
	 * @return Returns a new instance od a vector object which is a normalized version of the original
	 */
	public Vector normalize()
	{
		double length = this.length();
		return new Vector(this.x/length, this.y/length);
	}
	
	public void setX(final double x)
	{
		this.x = x;
	}
	public void setY(final double y)
	{
		this.y = y;
	}
	
	/**
	 * Sets location of a vector
	 * @param x diagonal coordinate of a new location
	 * @param y horizontal coordinate of a new location
	 */
	public void set(final double x, final double y)
	{
		this.setX(x);
		this.setY(y);
	}
	public double getX()
	{
		return this.x;
	}
	public double getY()
	{
		return this.y;
	}
	
}
