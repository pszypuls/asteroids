package pszypuls.asteroids.util;

/**
 * Class representing a point or a vector in a toroidal space.
 * Parameters X and Y are of type double ranging between 0 and 1.0
 * In this representation point (0,0) is located in the top left corner,
 * (1,1) is in bottom right.
 * 
 * @author Patryk Szypulski
 *
 */
public class ToroidVector extends Vector
{
	
	/**
	 * Constructor.
	 * @param x Value of a diagonal coordinate of a constructed vector
	 * @param y Value of a horizontal coordinate of a constructed vector
	 */
	public ToroidVector(final double x, final double y)
	{
		this.setX(x);
		this.setY(y);
	}
	
	
	/**
	 * Default constructor. Initializes both X and Y values to 0.
	 */
	public ToroidVector() 
	{
		this.setX(0);
		this.setY(0);
	}

	/**
	 * Constructor of a ToroidVector object.
	 * Can be used to convert a Vector object to ToroidVector object 
	 * @param vector Vector object used as an imprint for creating a new ToroidVector
	 */
	public ToroidVector(final Vector vector)
	{
		this.setX(vector.getX());
		this.setY(vector.getY());
	}
	
	/**
	 * Gets distance to other vector in a toroidal space
	 * 
	 */
	public double distance(final ToroidVector vector)
	{
		double differenceX = Math.abs(this.x - vector.getX());
		double differenceY = Math.abs(this.y - vector.getY());
		double distanceX = Math.min(differenceX, 1-differenceX);
		double distanceY = Math.min(differenceY, 1-differenceY);
		return Math.sqrt(distanceX*distanceX + distanceY*distanceY);
	}
	
	/**
	 * Overridden method for setting X coordinate. Result is always ranging between 0 and 1
	 */
	@Override
	public void setX(final double x)
	{
		this.x = x % 1.0;
		if (this.x < 0 )
		{
			this.x = 1 + this.x;
	
		}
	}
	/**
	 * Overridden method for setting Y coordinate. Result is always ranging between 0 and 1
	 */	
	@Override
	public void setY(final double y)
	{
		this.y = y % 1.0;
		if (this.y < 0)
		{
			this.y = 1 + this.y;
		}
	}
}
