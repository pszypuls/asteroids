package pszypuls.asteroids.util;

/**
 * Enum type describing possible states of a player ship's engine.
 * RUNNING - engine is turned on, ship is accelerating.
 * STOPPED - engine is turned off, ship is traveling at a constant speed, gained prior to turning off the engine.
 * @author patryk
 *
 */
public enum EngineStatus
{
	RUNNING(1), STOPPED(0);
	private int value;
	
	private EngineStatus(final int value)
	{
		this.value = value;
	}
	public int getValue()
	{
		return value;
	}
};
